package application.main;

import application.classes.Transport;
import framework.core.*;

public class AutowiringTest {
    private static GenericXmlApplicationContext context = new GenericXmlApplicationContext(AutowiringTest.class);
    static {
        context.load(MainApp.class.getResource("/GS_SpringXMLConfig.xml").getPath());
        context.setValidating(true);
        context.setParserType(XmlBeanDefinitionReader.ParserTypes.SAX);
    }

    private static class A {
        @Autowiring(value="bus")
        private Transport transport;


        @Autowiring
        private Transport car;

        public String toString() {
            return transport.toString() + " " + car.toString();
        }
    }

    public static void main(String[] args){
        A a = new A();
        try {
            AutowiringHandler.inject(a, context);
            System.out.println(a.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

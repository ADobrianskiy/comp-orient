package framework.core;

import framework.parsers.Bean;

import java.lang.reflect.Field;

public class AutowiringHandler {
    public static void inject(Object instance, GenericXmlApplicationContext context) throws Exception {
        Field[] fields = instance.getClass().getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(Autowiring.class)) {
                Autowiring annotation = field.getAnnotation(Autowiring.class);
                field.setAccessible(true); // should work on private fields
                try {
                    String beanId = !annotation.value().isEmpty() ? annotation.value() : field.getName();
                    Object bean = context.getBeanFactory().getBean(beanId);
                    if(bean == null) {
                        throw new Exception("No bean available with id '" + beanId + "'");
                    }
                    field.set(instance, bean);
                } catch (Exception e) {
                    String error = e.toString();

                    error = "Can not ceate obejct with class '" + instance.getClass().getSimpleName() +
                            "'. Field '" + field.getName() + "' initialization error:" + error;

                    throw new Exception(error);
                }
            }
        }
    }
}
